/*
 * Starscroll is a simple program that displays 
 * a scrolling number of stars to the user.
 * 
 * Number of stars can be changed by supplying
 * a desired number at runtime. Default number
 * is 400.
 *
 * eg: ./Starscroll 400
 */

#include <SDL2/SDL.h>
#include <time.h>
#include <stdbool.h>

/* Global Variables Definitions */
#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600

#define DEFAULT_STAR_COUNT 500

#define SIZE_RATIO  0.03f
#define VEL_RATIO 0.025f

#define MAX_STAR_SIZE 7 
#define MAX_DEPTH 255.0f 

#define TITLE "Starscroll"

SDL_Window *window = NULL;
SDL_Renderer *renderer = NULL;

typedef struct
{
	float x, y, z;
	float vel;
	float side_length;
    float r, g, b;
} Star;

bool init_sdl()
{
	bool success = 0;

	if (SDL_Init(SDL_INIT_VIDEO) == 0) {
		window = SDL_CreateWindow(TITLE, SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);

		if (window != NULL) {
			renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_PRESENTVSYNC);
			if (renderer == NULL) {
				success = 1;
				printf("Failed renderer initialization! %s\n", SDL_GetError());
			}
		} else {
			success = 1;
			printf("Could not initialize window! %s\n", SDL_GetError());
		}
	} else {
		success = 1;
		printf("Could not initialize SDL! %s\n", SDL_GetError());
	}
	return success;
}

void quit()
{
	if (renderer)
		SDL_DestroyRenderer(renderer);
	if (window)
		SDL_DestroyWindow(window);

	renderer = NULL;
	window = NULL;
	SDL_Quit();
}

void render(SDL_Rect *rect, const Star *stars, const int totalStars)
{
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
	SDL_RenderClear(renderer);
	
	for (int i = 0; i <= totalStars - 1; i++) {
    
        SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
                
		SDL_SetRenderDrawColor(renderer, stars[i].r, stars[i].g, stars[i].b, stars[i].z);
		rect->x = stars[i].x;
		rect->y = stars[i].y;
		rect->w = stars[i].side_length;
		rect->h = stars[i].side_length;

		SDL_RenderFillRect(renderer, rect);
	}
	SDL_RenderPresent(renderer);
}

float randomFloat(const int range)
{
	/* Returns a random floating point from 0 - max range */
	float f = (((float)rand() / RAND_MAX) * range);
	return f;
}

void resetStar(Star *s)
{
	/* Starting Variables for each star reset*/	
	s->y = randomFloat(SCREEN_HEIGHT);
	s->z = randomFloat(MAX_DEPTH);
    
    /* Here side length is going to be dependent on the depth of the stars' starting position * .3% */
	s->side_length = s->z * SIZE_RATIO;
	s->vel = (s->side_length + s->z) * VEL_RATIO;
	s->x = -s->side_length; //start at the negative side length so we have a smooth transition onto the screen

    s->r = randomFloat(255);
    s->g = randomFloat(255);
    s->b = randomFloat(255);
}

void start(Star *stars, const int totalStars)
{
	for (int i = 0; i < totalStars; i++) {
	
		resetStar(&stars[i]); 
		stars[i].x = randomFloat(SCREEN_WIDTH);
	}
}

void updatePosition(Star *stars, const int numberOfStars)
{
	for (int i = 0; i < numberOfStars; i++) {
		if (stars[i].x > (SCREEN_WIDTH) ||
		        (stars[i].x < -stars[i].side_length))
			resetStar(&stars[i]);

		stars[i].x += stars[i].vel;
	}
}

int main (int argc, char *argv[])
{	
	srand(time(NULL));
	int totalStars;

	if (argc >= 2 && atoi(argv[1]) > 0)
		totalStars = atoi(argv[1]);
	else
		totalStars = DEFAULT_STAR_COUNT;
	
	SDL_Rect r;
	Star stars[totalStars];
	
    if (init_sdl() == 0) {
		start(stars, totalStars);
		bool done = false;
		while (!done) {
			SDL_Event event;
			updatePosition(stars, totalStars);
			render(&r, stars, totalStars);

			while (SDL_PollEvent(&event)) {
				if (event.type == SDL_QUIT) {
					done = true;
				}
			}
		}
	} else {
		printf("Program will now Exit!\n");
		return 1;
	}
	quit();
}
