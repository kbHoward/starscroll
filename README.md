# Starscroll

## Description
Starscroll is a simple program written in C
that displays small squares that scroll 
across the screen.

The number of 'stars' you see on the screen 
can be changed by supplying console arguments
to the command line. 

## Dependencies
SDL2 and SDL2 development header files.

## Installation 

```
make
./Starscroll [int arguments]
```
