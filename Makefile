CC=gcc
CFLAGS=-Wall

SRC_DIR=src/
SRC=$(SRC_DIR)*.c
EXEC=Starscroll

OBJ=$(SRC_DIR)*.o
LIB=-lSDL2 -lm
orbit: $(OBJ)
	$(CC) $(CFLAGS) $(SRC) $(LIB) -o $(EXEC)

$(OBJ): $(SRC)
	$(CC) -c $(SRC)

.PHONY: clean
clean:
	rm *.o $(EXEC)
